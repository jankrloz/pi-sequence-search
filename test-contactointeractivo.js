/**
 * Search for a successive numbers into the PI with length NUM_OF_DIGITS, that sums TARGET
 * author: jankrloz
 */

// Getting PI with 500 digits from http://www.eveandersson.com/pi/digits/pi-digits?n_decimals_to_display=500&breakpoint=100
const strPI = `
3.
1415926535897932384626433832795028841971693993751058209749445923078164062862089986280348253421170679
8214808651328230664709384460955058223172535940812848111745028410270193852110555964462294895493038196
4428810975665933446128475648233786783165271201909145648566923460348610454326648213393607260249141273
7245870066063155881748815209209628292540917153643678925903600113305305488204665213841469519415116094
3305727036575959195309218611738193261179310511854807446237996274956735188575272489122793818301194912
`

// Create an array and skip newlines and no numbers(like point)
const PI = strPI.split('').filter(num => !isNaN(parseInt(num)))

const NUM_OF_DIGITS = 8 // Max number to search
const TARGET = 13 // Result of the sum between the numbers

for (let pivot = 0; pivot < PI.length; pivot += 1) {
    let sum = 0
    let results = []
    for (let i = 0; i < NUM_OF_DIGITS && i < PI.length; i += 1) {
        const currentDigit = parseInt(PI[pivot + i])
        sum += currentDigit
        results.push(currentDigit)
        if (sum >= TARGET) break; // Save iterations if the sum is greater or equal than SUM
    }
    if (sum === TARGET && results.length == NUM_OF_DIGITS) {
        console.log(`Sequence found that sums ${sum}: ${results} (in position ${pivot})`)
        return;
    }
}
